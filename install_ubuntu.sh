#!/bin/bash

rm ubuntu.raw
qemu-img create -f raw ubuntu.raw 30G

virt-install \
--virt-type kvm \
-n ubuntu \
--description "Ubuntu Linux" \
--os-type=Linux \
--os-variant=generic \
--ram=2048 \
--cpu host-model \
--disk path=ubuntu.raw,bus=virtio,size=30 \
--graphics vnc,listen=0.0.0.0 --noautoconsole \
--livecd \
--cdrom ubuntu.iso \
--boot cdrom \
--network bridge:virbr0 
